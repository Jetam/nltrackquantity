# NLTrackQuantity

## Overview
Warehouse wants to track incoming products from its suppliers. They want to use a client/server
application that tracks quantity of products in their warehouse for each of their suppliers. Each
supplier would have a desktop application that communicates with server. Whenever they send
product to the warehouse, they also increase the quantity of products in the application. Warehouse
uses server application to continuously monitor the quantities.

## Program Design
Server
Server side is a web page that displays quantities from all suppliers in a grid. Grid is automatically
updated as clients enter new data.
Database on the server contains one table (Customer) with three columns (Name, Password,
Quantity).
Web services are used to allow clients to authenticate user and increase quantity.
Client
Client side shows a login dialog at start. Supplier credentials (username and password) are verified on
the server and the main form opens.
Main form contains edit control and a button. Edit control is used to enter quantity and button to
send it to the server. When button is pressed, quantity is sent to the server and added to existing
quantity.

## Requirements
 Client side is created using WPF and MVVM pattern.
 Server side is Angular application (or ASP.NET MVC).
 The application should be developed using proper standards and code practices as if it was a
real-life application.
 Security concerns: Passwords should not be stored in plain text in the database or sent in
plain text over web services.
 Extra points if code is unit tested.
 Extra points for creativity and a tidy effective user interface.
